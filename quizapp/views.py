from django.shortcuts import render, get_object_or_404, redirect
#from django.http import HttpResponse
from .models import Quiz, Question, Topic, Registration, Team, Points
from .forms import TopicForm, QuizForm, QuestionForm, TeamForm, PointsForm, RegistrationForm
from datetime import datetime

def index(request):
    quizzes = Quiz.objects.all()
    return render(request, 'quizapp/index.html', {'quizzes':quizzes})

def quiz(request, quiz_id):
    quiz = get_object_or_404(Quiz,pk=quiz_id)
    topics = Topic.objects.filter(quiz=quiz)
    return render(request, 'quizapp/quiz.html', {'quiz':quiz, 'topics':topics})

def addtopic(request, quiz_id):
    quiz = get_object_or_404(Quiz,pk=quiz_id)
    if request.method == 'POST':
        topic_form = TopicForm(request.POST)
        if topic_form.is_valid():
            Topic(quiz=quiz, name=topic_form.cleaned_data['name'], description=topic_form.cleaned_data['description']).save()
            return redirect('quiz', quiz_id=quiz.id)

def newtopic(request, quiz_id):
    quiz = get_object_or_404(Quiz,pk=quiz_id)
    topic_form = TopicForm()
    return render(request, 'quizapp/newtopic.html', {'quiz':quiz, 'topic_form':topic_form})

def addquiz(request):
    if request.method == 'POST':
        quiz_form = QuizForm(request.POST)
        if quiz_form.is_valid():
            Quiz(start=quiz_form.cleaned_data['start'], end=quiz_form.cleaned_data['end']).save()
            return redirect('index')

def newquiz(request):
    quiz_form = QuizForm()
    return render(request, 'quizapp/newquiz.html', {'quiz_form':quiz_form})

def editquiz(request, quiz_id):
    quiz = get_object_or_404(Quiz, id=quiz_id)
    form = QuizForm(request.POST or None, instance=quiz)
    if form.is_valid():
        form.save()
        return redirect('index')
    return render(request, 'quizapp/editquiz.html', {'quiz_form': form, 'quiz':quiz})

def deletequiz(request, quiz_id):
    quiz = Quiz.objects.get(id=quiz_id)
    quiz.delete()
    return redirect('index')

def topic(request, topic_id):
    topic = get_object_or_404(Topic,pk=topic_id)
    questions = Question.objects.filter(topic=topic)
    return render(request, 'quizapp/topic.html', {'topic':topic, 'questions':questions})

def edittopic(request, topic_id):
    topic = get_object_or_404(Topic, id=topic_id)
    form = TopicForm(request.POST or None, instance=topic)
    if form.is_valid():
        form.save()
        return redirect('quiz', quiz_id=topic.quiz.id)
    return render(request, 'quizapp/edittopic.html', {'topic_form': form, 'topic':topic})

def deletetopic(request, topic_id):
    topic = Topic.objects.get(id=topic_id)
    topic.delete()
    return redirect('quiz', quiz_id=topic.quiz.id)

# def newquestion(request, topic_id):
#     topic = get_object_or_404(Topic, id=topic_id)
#     form = QuestionForm(request.POST)
#     if form.is_valid():
#         Question(
#             topic=topic,
#             question_text=form.cleaned_data['question_text'],
#             answer=form.cleaned_data['answer'],
#             max_points=form.cleaned_data['max_points']
#         ).save()
#         return redirect('topic', topic_id=topic.id)
#         form.save()
#     return render(request, 'quizapp/newquestion.html', {'question_form': form, 'topic':topic})

def addquestion(request, topic_id):
    topic = get_object_or_404(Topic,pk=topic_id)
    if request.method == 'POST':
        question_form = QuestionForm(request.POST)
        if question_form.is_valid():
            Question(
                topic=topic,
                question_text=question_form.cleaned_data['question_text'],
                answer=question_form.cleaned_data['answer'],
                max_points=question_form.cleaned_data['max_points']
            ).save()
            return redirect('topic', topic_id=topic.id)

def newquestion(request, topic_id):
    topic = get_object_or_404(Topic,pk=topic_id)
    question_form = QuestionForm()
    return render(request, 'quizapp/newquestion.html', {'question_form': question_form, 'topic':topic})

def question(request, question_id):
    question = get_object_or_404(Question,pk=question_id)
    return render(request, 'quizapp/question.html', {'question':question})

def editquestion(request, question_id):
    question = get_object_or_404(Question, id=question_id)
    form = QuestionForm(request.POST or None, instance=question)
    if form.is_valid():
        form.save()
        return redirect('question', question_id=question.id)
    return render(request, 'quizapp/editquestion.html', {'question_form': form, 'question':question})

def deletequestion(request, question_id):
    question = Question.objects.get(id=question_id)
    question.delete()
    return redirect('topic', topic_id=question.topic.id)

def teams(request):
    teams = Team.objects.all()
    return render(request, 'quizapp/teams.html', {'teams':teams})

def addteam(request):
    if request.method == 'POST':
        team_form = TeamForm(request.POST)
        if team_form.is_valid():
            Team(
                name=team_form.cleaned_data['name'],
                captain_name=team_form.cleaned_data['captain_name'],
                email=team_form.cleaned_data['email'],
                captain_surname=team_form.cleaned_data['captain_surname'],
                phone_number=team_form.cleaned_data['phone_number'],
                number_of_members=team_form.cleaned_data['number_of_members']
                ).save()
            return redirect('teams')

def newteam(request):
    team_form = TeamForm()
    return render(request, 'quizapp/newteam.html', {'team_form':team_form})

def team(request, team_id):
    team = get_object_or_404(Team,pk=team_id)
    registrations = Registration.objects.filter(team=team)
    upcoming_quizzes = [x.quiz for x in registrations if x.quiz in Quiz.objects.filter(start__gte=datetime.now())]
    passed_quizzes = [x.quiz for x in registrations if x.quiz in Quiz.objects.filter(start__lt=datetime.now())]
    return render(request, 'quizapp/team.html', {'team':team, 'upcoming_quizzes':upcoming_quizzes, 'passed_quizzes':passed_quizzes})

def editteam(request, team_id):
    team = get_object_or_404(Team, id=team_id)
    form = TeamForm(request.POST or None, instance=team)
    if form.is_valid():
        form.save()
        return redirect('team', team_id=team.id)
    return render(request, 'quizapp/editteam.html', {'team_form': form, 'team':team})

def deleteteam(request, team_id):
    team = Team.objects.get(id=team_id)
    team.delete()
    return redirect('teams')

def registrations(request, team_id):
    team = get_object_or_404(Team,pk=team_id)
    registrations = Registration.objects.filter(team=team)
    upcoming_quizzes = Quiz.objects.filter(start__gte=datetime.now())
    quizzes_registered = [x for x in upcoming_quizzes if x in [y.quiz for y in registrations]]
    quizzes_not_registered = [x for x in upcoming_quizzes if x not in quizzes_registered]
    return render(request, 'quizapp/registrations.html', {'team':team, 'quizzes_registered':quizzes_registered, 'quizzes_not_registered':quizzes_not_registered})

def registrationsbyquiz(request, quiz_id):
    quiz = get_object_or_404(Quiz,pk=quiz_id)
    registrations = Registration.objects.filter(quiz=quiz)
    teams = Team.objects.all()
    teams_registered = [x.team for x in registrations]
    teams_not_registered = [x for x in teams if x not in teams_registered]
    return render(request, 'quizapp/registrationsbyquiz.html', {'teams_registered':teams_registered, 'teams_not_registered':teams_not_registered, 'quiz':quiz})

def addregistration(request, team_id, quiz_id, view_quiz):
    team = get_object_or_404(Team,pk=team_id)
    quiz = get_object_or_404(Quiz,pk=quiz_id)
    if quiz.end.replace(tzinfo=None) > datetime.now():
        Registration(
            team=team,
            quiz=quiz,
            time=datetime.now()
        ).save()
    if view_quiz == 1:
        return redirect('registrationsbyquiz', quiz_id = quiz.id)
    return redirect('registrations', team_id = team.id)

def deleteregistration(request, team_id, quiz_id, view_quiz):
    team = get_object_or_404(Team,pk=team_id)
    quiz = get_object_or_404(Quiz,pk=quiz_id)
    registration = get_object_or_404(Registration,team=team,quiz=quiz)
    if quiz.end.replace(tzinfo=None) > datetime.now():
        registration.delete()
    if view_quiz == 1:
        return redirect('registrationsbyquiz', quiz_id = quiz.id)
    return redirect('registrations', team_id = team.id)


def addpoints(request, question_id):
    question = get_object_or_404(Question,pk=question_id)
    registrations = Registration.objects.filter(quiz=question.topic.quiz)
    teams = [x.team for x in registrations]
    if request.method == 'POST':
        form = PointsForm(request.POST, teams=teams, question=question)
        if form.is_valid():
            all_fields = form.fields.keys()
            for field in all_fields:
                team = get_object_or_404(Team,pk=int(field))
                try:
                    p = Points.objects.get(team=team, question=question)
                    p.number_of_points = form.cleaned_data[field]
                    p.save()
                except Points.DoesNotExist:
                    Points(
                        question=question,
                        team=team,
                        number_of_points=form.cleaned_data[field]
                    ).save()
            return redirect('question', question_id=question.id)
    return redirect('index')

def newpoints(request, question_id):
    question = get_object_or_404(Question,pk=question_id)
    registrations = Registration.objects.filter(quiz=question.topic.quiz)
    teams = [x.team for x in registrations]
    points_form = PointsForm(teams=teams, question=question)
    return render(request, 'quizapp/newpoints.html', {'points_form': points_form, 'question':question})

def results(request, quiz_id):
    quiz = get_object_or_404(Quiz,pk=quiz_id)
    points = [x for x in Points.objects.all() if x.question.topic.quiz == quiz]
    teams = [x.team for x in Registration.objects.all() if x.quiz == quiz]
    results = []
    for team in teams:
        results.append({'team':team,'points':sum([x.number_of_points for x in points if x.team == team])})
    results.sort(key=lambda x: x['points'], reverse=True)
    for i, res in enumerate(results):
        res['position'] = i+1
    return render(request, 'quizapp/results.html', {'quiz':quiz, 'results':results})

def createaddregistration(request):
    if request.method == 'POST':
        registration_form = RegistrationForm(request.POST)
        if registration_form.is_valid():
            if not Registration.objects.filter(quiz=registration_form.cleaned_data['quiz'], team=registration_form.cleaned_data['team']).exists():
                Registration(
                    quiz=registration_form.cleaned_data['quiz'],
                    team=registration_form.cleaned_data['team'],
                    time=datetime.now()
                    ).save()
        return redirect('createnewregistration')

def createnewregistration(request):
    registration_form = RegistrationForm()
    return render(request, 'quizapp/createnewregistration.html', {'registration_form':registration_form})
