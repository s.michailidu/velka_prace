# Velká práce

Projekt do předmětu SSP - velká práce. Django projekt na téma web pro správu kvízů.

## Instalace

1. Instalace balíčků
    ```
    pip install -r requirements.txt
    ```
1. Run
    ```
    python manage.py runserver
    ```

